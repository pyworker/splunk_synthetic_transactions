# Синтетические транзакции

Синтетическая транзакция запускается на клинской машине пользователя, на которой запущен Splunk Forwarder. Транзакция зарускается на нескольких машинах (оюъединенных в кластер) и данные со всех машин поступают в единый индекс (synth). 

## Запуск новой синтетической транзакции

1. Установить пакет `xvfb` для возможности работы с виртуальным экраном 

```bash
sudo apt-get install xvfb
```

2. При необходимости установить все необходимые python пакеты (в случае если их нет в директории приложения)

```bash
$ python -m pip install --upgrade pip
$ python -m pip install selenium
$ python -m pip install pyvirtualdisplay
$ python -m pip install user_agents
```

2. Скачать драйвер geckodriver и переместить в директорию `/urs/bin`

```bash
$ wget https://github.com/mozilla/geckodriver/releases/download/v0.20.0/geckodriver-v0.20.0-linux64.tar.gz
$ tar zxvf geckodriver-v0.20.0-linux64.tar.gz
$ mv geckodriver /usr/bin
```


3. Записать новую синтетическую транзакцию с помощью Selenium UI (Firefox plugin)
4. Записанную транзакцию экспортировать в питоновский код и сохранить в файл с расширением `.py`

```python

# add imports 
from transaction import Transaction
from transactionstep import TransactionStep
from pyvirtualdisplay import Display


class TestWebUITransaction(unittest.TestCase)
    def setUp(self):
        # add two lines before all other lines
        self.display = Display(visible=0, size=(1366, 768))
        self.display.start()
            .   .   .

    def test_transaction_function(self):
        driver = self.driver
        # before run transaction
        with Transaction(driver=driver, name='<Transaction name>') as tr:
            # go to website
            with TransactionStep(transaction=tr, name='<TransactionStep name>'):
                driver.get("https://music.yandex.ru/home")
                            .   .   .

    def tearDown(self):

        # after all comands
        self.display.stop()


if __name__ == "__main__":
    unittest.main()

```


5. Добавить в код синтетической транзакции директивы 

5. Подготовить новый bash для запуска поготовленной транзакции. При этом скрипт должен иметь следющее содержание

```bash
#!/bin/bash
unset PYTHONPATH
unset LD_LIBRARY_PATH
CLASSPATH="/opt/splunk/selenium-server-standalone-*.jar"
export CLASSPATH
SELENIUM_SERVER_JAR="//opt/splunk/selenium-server-standalone-*.jar"
export SELENIUM_SERVER_JAR

python3.5 /opt/splunk/etc/apps/splunk-app-synthetic/bin/music_trasaction.pys
```

В случае с windows подготавливается файл `.path` со следующим содержанием
```
"$SPLUNK_HOME\bin\splunk.exe" cmd python $SPLUNK_HOME\etc\apps\alert_manager\bin\alert_manager_scheduler.py
```

6. Разрешить запуск скрипт с транзакцией 
```
$ chmod u+=rwx start_music_transaction.sh
```

7. Можно убедиться в успешности составления сценария запуска
```
$ . start_music_transaction.sh
```


## Настройка data inputs

Запуском скрипта можно управлять как с использованием Splunk UI, так и с использованием конфигурационных файлов. Так как в большинстве случаев синтетическая транзакция функционирует на склинской машине, графического интерфейса там может и не быть, поэтому наиболее предпочтительный способ управления запусками скрипта становится: управление через конфигурационные файлы. 

### Управление запуском синтетической транзакции через Splunk UI

1. В Splunk UI перейти в настройки
2. Выбрать пункт *Data Inputs*
3. Выбрать *Add new data input*
4. Для нового input указать
    + выбрать запускаемый скрипт синтетической транзакции
    + время запуска минтетической транзакции: 60 с 
    + sourcetype: `dmesg`
    + app: Splunk Synthetic Application
    + index: `synth` (это кластерный индекс)

### Управление запуском синтетической транзакции через inputs.conf

1. В директории local приложения создать файл с названием `inputs.conf` 
2. В файл `local/inputs.conf` добавить следующий код запуска синтетической транзакции

```
[script://$SPLUNK_HOME\etc\apps\splunk_synthetic_transactions\bin\transactionscript.py]
disabled = false
index = synth
interval = 60.0
sourcetype = dmesg
```

При этом логирование работы приложения можно осуществлять в локальнеую директорию, после чего включить монторинг этих лог-файлов

```
[monitor://$SPLUNK_HOME\etc\apps\splunk_synthetic_transactions\bin\log]
disabled = false
index = synth
interval = 60.0
sourcetype = dmesg
source = yandex_transaction
```


## Настройка индекса

Для работы синтетических транзакций необходимо наличие индекса `synth` в который записываются отчеты о выполнении синтетических транзакций. **Важно для выполнения синтетической транзакции требуется кластерный index. Кластерный индекс создается на cluster master руками.**

Для создания некластерного индекса достаточно прописать его в `default/indexes.conf` директории проекта. В этом файле каждый индекс опичывается следующим образом:

```
[synth]
coldPath = $SPLUNK_DB/synth/colddb
homePath = $SPLUNK_DB/synth/db
thawedPath = $SPLUNK_DB/synth/thaweddb
```

где `synth` - это название индекса



## Диплой приложения

1. Директория с приложением, которое предстоит задеплоить на кластер, помещается в `/opt/splunk/deployment-apps` на хосте deployment server.
2. Далее использую Splunk UI выполняется переход в `Settings => Forwarder Manager`
    + Во вкладке `Clients` отоброжаются все клиенты управляемые текущим deployment server.
    + Вкладка `Server Classes` использкется для управления классами. Класс - ассоциация приложения с одним или более хостов. 
    + Вкладка `Apps` содержит все приложения, которые можно задеплоить на кластер (приложения, которые находятся в `/opt/splunk/deployment-apps`).

Порядок Деплоя приложения

1. На вкладке `Server Classes` создается новый класс (`New Server Class`) В котором указывается набор хостов для приложения
2. Во вкладке `Apps` выбираем нужное приложение и нажимаем `Edit`. 
3. В появившемся окне выбираем нужный `Server Classes` и ставим галочки напротив `Enable App` и `Restart Splunkd` и жмем кнопку `Save`


## Подготовка окружения

Создание виртуального окружения и установка всех необходимых python-бибилиотек определенных в файле `requirements.txt`.  

```bash
$ virtualenv --python=python3.6 myvenv
$ source myvenv/bin/activate
(myvenv)$ python -m pip install --upgrade pip 
(myvenv)$ python -m pip install -r requirements.txt
```

Определение разрядности ОС и установка необходимого драйвера `geckodriver` 
(с учетом версии дистрибутива). Перемщение драйвер `geckodriver` в каталог `/usr/bin/` (в данном случае в директорию `<название переменного окружения>/bin`)

```bash
(myvenv)$ uname -m 
x86_64  # 64-битная версия
i686    # 32 битная версия

(myvenv)$ wget https://github.com/mozilla/geckodriver/releases/download/v0.20.0/geckodriver-v0.20.0-linux64.tar.gz
(myvenv)$ tar zxvf geckodriver-v0.20.0-linux64.tar.gz
(myvenv)$ mv geckodriver selen/bin/
```
 
Начало выполнения сентетических транзакций. Сценарций транзакции описан в `music_trasaction.py`.
```bash
$ python -m unittest music_trasaction.py
```


## Библиотека X virtual framebuffer 

**Xvfb** (X virtual framebuffer) — виртуальный X-сервер, который для вывода использует не видеокарту, а оперативную память.  Преимущества виртуального дисплея в том, что он не требует установки графического окружения, унифицирует системы, не требует авторизации пользователя в ОС и доступа к его X-сессии.

Для конфигурирования экрана при выполнении теста используется библиотека `pyvirtualdisplay`

Установка 
```bash
$ sudo apt-get install xvfb
```


Установка библиотеки 
```bash
$ python -m pip install pyvirtualdisplay
```

Использование библиотеки 
```python
from pyvirtualdisplay import Display

# Set screen resolution to 1366 x 768 like most 15" laptops
display = Display(visible=0, size=(1366, 768))
display.start()

# ...
# work with selenium
# ...

# quit virual display
display.stop()
```



Полный интеграционный пример [source](http://www.vionblog.com/selenium-headless-firefox-webdriver-using-pyvirtualdisplay/)
```python
#!/usr/bin/env python

from pyvirtualdisplay import Display
from selenium import webdriver

# Set screen resolution to 1366 x 768 like most 15" laptops
display = Display(visible=0, size=(1366, 768))
display.start()

# now Firefox will run in a virtual display.
browser = webdriver.Firefox()

# Sets the width and height of the current window
browser.set_window_size(1366, 768)

# Open the URL
browser.get('http://www.vionblog.com/')

# set timeouts
browser.set_script_timeout(30)
browser.set_page_load_timeout(30) # seconds

# Take screenshot
browser.save_screenshot('vionblog.png')

# quit browser
browser.quit()

# quit Xvfb display
display.stop()
```


## Git Cheatsheet
Откатить последний commit, сохранив при этом изменения (сделанные до коммита и после клммита)
```bash
$ git reset --soft HEAD^
```

Откатить последний commit удалив при этом все изменения
```bash
$ git reset --hard HEAD^
```

Убрать из индекса git файлы (undo git add). Если не указывать <file> то из индекса будут удалены все файлы.
```bash
$ git reset [<file>]
```
