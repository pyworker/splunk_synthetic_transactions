# Установка расширения Katalon Recorder в браузер Mozilla FireFox

1. Открыть браузер Mozilla FireFox 
2. Открыть директорию с katalon_recorder_selenium_ide_for_firefox_55-3.4.11-an+fx-windows.xpi
3. Выделить katalon_recorder_selenium_ide_for_firefox_55-3.4.11-an+fx-windows.xpi и перенести в Firefox.
4. Расширение Katalon Recorder установлено в браузер Mozilla FireFox
