# Установка Chrome плагина Katalon-Recorder

1. Распаковать архив `Katalon-Recorder.zip`
2. Запустить брузер Google Crome
3. Перейти по адресу `chrome://extensions/`
4. В правом верхнем углу включить "Режим разработчика"
5. В верхней панели выбрать "Загрузить распакованные расширения"
6. Указать адрес до директории с распакованным `Katalon-Recorder`
7. Плагин `Katalon-Recorder` установлен в Google Chrome
