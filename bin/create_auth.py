#!/usr/bin/env python2.7

"""
module for creating a user account
"""


from getpass import getpass, getuser
from sys import stdout, exit
from os.path import exists
from os import chmod, remove, path
from stat import S_IRUSR
from secret import SymEncryptWin
from secret import SecretKey

HASH_PATH = '.secret'
AUTH_PATH = '.auth'


def main():
    dir_path = path.dirname(path.realpath(__file__))
    ap = '/'.join([dir_path, AUTH_PATH])
    hp = '/'.join([dir_path, HASH_PATH])

    if not exists(hp):
        print('Error: need to create secret key: run "python create_key"')
        exit(1)

    if exists(ap):
        while(True):
            stdout.write('Auth already set: delete it (y/n) ')
            choice = raw_input().lower()
            if choice == 'y':
                remove(ap)
                break
            elif choice == 'n':
                exit()


    stdout.write('Login: ')

    # enter login 
    login = raw_input()

    # enter password
    upass = getpass()
    vupass = getpass('Verify password: ')
    if upass != vupass:
        print('Error: passwords do not match')
        exit(1)

    sew = SymEncryptWin()
    sk = SecretKey()
    key = sk.get()

    try:
        with open(ap, 'wb') as f:
            f.write(':'.join([login, sew.encrypt(source=upass, key=key)]))
        chmod(ap, S_IRUSR)
    except Exception as e:
        raise e


if __name__ == '__main__':
    main()