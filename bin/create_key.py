#!/usr/bin/env python2.7

"""
module for creating a secret key
"""


from getpass import getpass, getuser
from sys import exit, stdout
from os import remove, path
from os.path import exists
from secret import SecretKey

HASH_PATH = '.secret'

def main():

    dir_path = path.dirname(path.realpath(__file__))
    hp = '/'.join([dir_path, HASH_PATH])
    if exists(hp):
        while(True):
            stdout.write('Secret key  already set: delete it (y/n) ')
            choice = raw_input().lower()
            if choice == 'y':
                remove(hp)
                break
            elif choice == 'n':
                exit()

    # get current user
    # user = getuser()

    upass = getpass()
    vupass = getpass('Verify password: ')
    if upass != vupass:
        print('Error: passwords do not match')
        exit(1)

    sk = SecretKey()
    sk.generate_key(password=upass, fname=hp)

    print('Secret key created')


if __name__ == '__main__':
    main()