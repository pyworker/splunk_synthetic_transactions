
#### STEP 1
## add all imports (lines starting with words "imports" and "imports")

import re
import time
import unittest
from sys import exit, platform
from logger import logger_setup
from transaction import Transaction
from transactionstep import TransactionStep
from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from os.path import dirname, abspath
from selenium.webdriver.chrome.options import Options
from time import sleep
from user import User


# check linux system 
# (can be linux or linux2)
LINUX_PLATFORM = re.compile('^linux')
DRIVERS_DIRECTORY = 'drivers'


class TemplateTransactionTest(unittest.TestCase):

    ## add setUp function
    def setUp(self):
        """call before each test function"""
        self.log = logger_setup(logger='synthetic_script')

        # # work with virtual display
        # # wirtual display can be used only on Linux systems
        # self.display = Display(visible=0, size=(1366, 768))
        # self.display.start()

        if LINUX_PLATFORM.match(platform): 
            self.log.info('use linux chrome driver')
            CHROME_DRIVER = 'chromedriver' 
        else:
            self.log.info('use windows chrome driver')
            CHROME_DRIVER = 'chromedriver.exe'

        app_dir = dirname(abspath(__file__))
        CHROMEDRIVER_PATH = r'/'.join([app_dir, DRIVERS_DIRECTORY, CHROME_DRIVER])
        chrome_options = Options()  
        chrome_options.add_argument("--headless") 
        self.log.info('prepare web driver for transaction %s' % __file__)

        try:
            # allow use chrome firefox and safari driver)
            # need to set path to driver
            self.driver = webdriver.Chrome(
                            executable_path=CHROMEDRIVER_PATH,
                            chrome_options=chrome_options)

            self.driver.implicitly_wait(30)

        except Exception as ex:
            err_msg = 'failed to run transaction %s: %s' % (__file__, ex)
            self.log.error(err_msg)
            exit(1)

        self.log.info('start transaction %s' % __file__)
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_synthetic_transaction(self):
        driver = self.driver
        user = User()
        # user.login
        # user,password

        # RENAME TRANSACTION NAME (name=' ' )
        with Transaction(driver=driver, name='your transaction name') as transaction:

            # ADD FIRST STEP AND RENAME IT (name=' ' )
            with TransactionStep(transaction=transaction, name='your step 1 name'):
                # driver.get("https://www.google.com/")
                pass

            # IF YOU NEED ADD SECOND STEP AND RENAME IT (name=' ' )
            with TransactionStep(transaction=transaction, name='your step 2 name'):
                # ADD SECOND STEP
                # driver.find_element_by_id("lst-ib").click()
                pass
   
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True 

    def tearDown(self):
        self.log.info('prepare to close transaction %s' % __file__)
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

        # # quit virtual display
        # self.display.stop()

        self.log.info('end transaction %s' % __file__)


#### STEP 5
## add this script as data input => sripts in SPLUNK UI
if __name__ == "__main__":
    unittest.main()
