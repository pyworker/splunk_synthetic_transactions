# -*- coding: utf-8 -*-

# modify script
from transaction import Transaction
from transactionstep import TransactionStep
from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re


class YandexMusicTransaction(unittest.TestCase):
    def setUp(self):
        """call before each test function"""
        # work with virtual display
        self.display = Display(visible=0, size=(1366, 768))
        self.display.start()

        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_yandex_music(self):
        driver = self.driver
        
        with Transaction(driver=driver, name='Yandex Music') as transaction:
            # go to website
            with TransactionStep(transaction=transaction, name='go to website'):
                driver.get("https://music.yandex.ru/home")

            # search music group
            with TransactionStep(transaction=transaction, name='search music group'):
                driver.find_element_by_id("nb-2").click()
                driver.find_element_by_id("nb-2").clear()
                driver.find_element_by_id("nb-2").send_keys("korn")
                driver.find_element_by_id("nb-2").send_keys(Keys.ENTER)
                # raise Exception('invalid transaction')
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        """call after each test function"""
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

        # quit virtual display
        self.display.stop()

if __name__ == "__main__":
    unittest.main()
