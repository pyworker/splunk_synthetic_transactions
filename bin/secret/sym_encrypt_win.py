"""
module for symmetric encryption
working on Windows without installation optional libs
"""

import pyaes
from hashlib import md5
from base64 import b64encode, b64decode

PATH_AUTH = '.auth'


class SymEncryptWin(object):
    """docstring for SymEncrypt"""
    def __init__(self):
        pass

    def encrypt(self, key, source):
        """
        encrypt source with key and write to auth file
        Args:
           key: str, key for encrypt
           login: str, user login
           source: text, plain password
        """
        aes = pyaes.AESModeOfOperationCTR(key)
        ciphertext = aes.encrypt(source.encode('utf-8'))
        ciphertext = b64encode(ciphertext).decode('latin-1')

        # with open('PATH_AUTH', 'w') as f:
        #     auth_str = ':'.join([login, ciphertext])
        #     f.write(ciphertext)
        return ciphertext

    def decrypt(self, key, source):
        """
        decrypt source with key
        """
        aes = pyaes.AESModeOfOperationCTR(key)
        ciphertext = b64decode(source.encode('latin-1'))
        decrypted = aes.decrypt(ciphertext)
        return decrypted
        
        