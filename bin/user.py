from os.path import exists
from os import path
from secret import SymEncryptWin, SecretKey

HASH_PATH = '.secret'
AUTH_PATH = '.auth'


class User(object):
    """docstring for User"""
    def __init__(self):
        self._login = None
        self._password = None
        dir_path = path.dirname(path.realpath(__file__))
        self._ap = '/'.join([dir_path, AUTH_PATH])
        self._hp = '/'.join([dir_path, HASH_PATH])
        try:
            self._parse_auth()
        except Exception as e:
            print(e)

    @property
    def login(self):
        return self._login

    @property
    def password(self):
        """
        return password from AUTH_FILE
        """
        return self._password

    def _parse_auth(self):
        if not exists(self._ap):
            print('Error: need to set auth: run "python create_auth"')

        with open(self._ap, 'r') as f:
            auth = f.read()

        if not auth:
            print('failed to get auth')

        sew = SymEncryptWin()
        k = SecretKey()

        auth = auth.split(':', 1)
        self._login = auth[0]
        password = auth[1]
        # print(login)
        # print(password)


        key = k.get(fname=self._hp)
        self._password = sew.decrypt(key=key, source=password)

        # print(password)


if __name__ == '__main__':
    user = User()
    print(user.login)
    print(user.password)