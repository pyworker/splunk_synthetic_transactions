"""
module for work with transaction step
python3.6
python2.7
"""

from time import time, strftime, gmtime
from transaction import Transaction

TRANSACTIONSTEP = (
    '"%(current_time)s\" event_type=\"step end\"'
    'transactionstep_start=\"%(transactionstep_start)s\" '
    'transactionstep_end=\"%(transactionstep_end)s\" ' 
    'transactionstep_name=\"%(transactionstep_name)s\" '
    'transactionstep_num=\"%(transactionstep_num)s\" '
    'transactionstep_end_epoch=\"%(transactionstep_end_epoch)s\" '
    'transactionstep_start_epoch=\"%(transactionstep_start_epoch)s\" '
    'transactionstep_duration=\"%(transactionstep_duration).4s\" '
    'transactionstep_status=\"%(transactionstep_status)s\" '
    'transactionstep_error=\"%(transactionstep_error)s\"'
    'execution_id=\"%(execution_id)s\" '
    'transaction_name=\"%(transaction_name)s\" ')

class TransactionStep(Transaction):
    """used to describe each step of a transaction"""
    def __init__(self, name, transaction):
        """
        Args:
            name: str, transaction step's name
            transaction: Transaction, Transaction parent to this step
        Raises:
            None 
        """
        if transaction:
            # super(TransactionStep, self).__init__(obj=transaction)
            super(TransactionStep, self).__init__(obj=transaction)
        
        self.step_name = name
        self.start_step_epoch = time()
        self.start_step = str(strftime(
                            "%Y-%m-%d %H:%M:%S",
                            gmtime(self.start_step_epoch)))

    @property
    def elapsed(self):
        """calculate transaction step elapsed"""
        return time() - self.start_step_epoch
        
    def __enter__(self):
        """call when run 'with' function"""
        return self

    def __exit__(self, error_type, value, error_traceback):
        """call when go out 'with' function"""
        # transactionstep_status = Transaction._transaction_is_done(type=type)
        transactionstep_status = Transaction._transaction_is_done(type=error_type)
        
        # get transaction error
        transactionstep_error = []
        if transactionstep_status:
            # add step to transaction only if it have status DONE 
            self.add_trasactionstep(name=self.step_name)
        else:
            transactionstep_error = Transaction._traceback_formatter(trace=error_traceback)
        
        current_time_epoch = time()
        current_time = str(strftime(
                            "%Y-%m-%d %H:%M:%S",
                            gmtime(current_time_epoch)))

        print(TRANSACTIONSTEP % ({
                'current_time': current_time,
                'transactionstep_start_epoch': self.start_step_epoch,
                'transactionstep_start': self.start_step,
                'transactionstep_end': current_time,
                'transactionstep_end_epoch': current_time_epoch, 
                'execution_id': self.execution_id,
                'transactionstep_name': '. '.join([str(self._steps_count), self.step_name]),
                'transactionstep_num': self._steps_count,
                'transactionstep_duration': self.elapsed,
                'transactionstep_status': transactionstep_status,
                'transactionstep_error': transactionstep_error,
                'transaction_name': self.app_name}))
