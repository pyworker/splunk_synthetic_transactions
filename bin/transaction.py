"""
module for work with transaction
python3.6
python2.7
"""

import traceback
from uuid import uuid1
from user_agents import parse
from time import time, strftime, gmtime
from socket import gethostbyname, gethostname


TRANSACTION = (
    "%(current_time)s event_type=\"transaction end\" "
    "transaction_end=\"%(transaction_end)s\" "
    "transaction_name=\"%(transaction_name)s\" "
    "transaction_end_epoch=\"%(transaction_end_epoch)s\" "
    "transaction_duration=\"%(transaction_duration)s\" "
    'transaction_status=\"%(transaction_status)s\" '
    'transaction_error=\"%(transaction_error)s\"'
    "execution_id=\"%(execution_id)s\" "
    "browser=\"%(browser)s\" "
    "browser_version=\"%(browser_version)s\" "
    "os=\"%(os)s\" "
    "os_version=\"%(os_version)s\" "
    "ip=\"%(ip)s\" "
    "title=\"%(title)s\" "
    "app_name=\"%(app_name)s\" "
    "steps_count=\"%(steps_count)s\""
    "steps=\"%(transaction_list)s\"")


class Transaction(object):
    """used to describe transaction"""
    def __init__(self, driver=None, name=None, obj=None):
        """
        USAGE: 
            - set field 'name' when need to create new object
            - set field 'obj' when need to copy object
        Args:
            driver: selenium.webdriver, browser handle 
            name: str, transaction name
            obj: Transaction, existing transaction object
        Reaises:
            ValueError: neither "name" nor "obj" set
        """
        if name:
            # this case calls from constuctor
            ua = driver.execute_script("return navigator.userAgent")
            user_agent = parse(ua)
            self.driver = driver
            self.start = time()
            self.execution_id = uuid1()
            self.app_name = name
            self.browser_version = user_agent.browser.version_string
            self.browser = user_agent.browser.family
            self.os = user_agent.os.family
            self.os_version = user_agent.os.version_string
            self.ip = gethostbyname(gethostname())
            self.tx_count = 0
            self.transaction_list = []
            self._steps_count = 0
        elif obj:
            # this case calls from transaction steps
            self.transaction_list = obj.transaction_list
            self.execution_id = obj.execution_id
            self._steps_count = obj.steps_count
            self.app_name = obj.app_name
        else:
            raise ValueError('args "name" and "object" not set')

    @property
    def elapsed(self):
        """calculate transaction elapsed"""
        return time() - self.start

    @property
    def steps_count(self):
        self._steps_count += 1
        return self._steps_count

    @staticmethod
    def _transaction_is_done(type):
        """
        check transaction status
        Args:
            type: Exception or None, type of raises Exception
        """
        if not type:
            return True
        else:
            return False

    @staticmethod
    def _traceback_formatter(trace):
        """
        set traceback format
        Args:
            error_traceback: Traceback, info about error
        """
        return traceback.format_tb(trace)

    def add_trasactionstep(self, name):
        """
        add info about new step in transaction
        Args:
            name: str, name of transaction step
        Returns:
            None
        """
        self.transaction_list.append(name)


    def __enter__(self):
        """call when run 'with' function"""
        return self

    def __exit__(self, error_type, value, error_traceback):
        """call when go out 'with' function"""
        transaction_status = self._transaction_is_done(type=error_type)
        
        # get transaction error
        transaction_error = []
        if not transaction_status:
            transaction_error = self._traceback_formatter(trace=error_traceback)
        
        current_time_epoch = time()
        current_time = str(strftime(
                            "%Y-%m-%d %H:%M:%S",
                            gmtime(current_time_epoch)))

        print(TRANSACTION % ({
            'current_time': current_time,
            'transaction_end': current_time,
            'transaction_name': self.app_name,
            'transaction_end_epoch': current_time_epoch,
            'transaction_duration': self.elapsed,
            'transaction_status': 'DONE' if transaction_status else 'False',
            'transaction_error': transaction_error,
            'execution_id': str(self.execution_id),
            'browser': self.browser,
            'browser_version': self.browser_version,
            'os': self.os,
            'os_version': self.os_version,
            'ip': self.ip,
            'title': self.driver.title,
            'app_name': self.app_name,
            'steps_count': self._steps_count,
            'transaction_list': self.transaction_list}))

        # return True for suppressed any exception 
        return True
